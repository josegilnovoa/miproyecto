package web;

import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ConsultaTvs", urlPatterns = {"/ConsultaTvs"})
public class ConsultaTvs extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>\n" +
"<html>\n" +
"<head>\n" +
"    <meta charset='utf-8'>\n" +
"    <meta http-equiv='X-UA-Compatible' content='IE=edge'>\n" +
"    <title>Listado de productos</title>\n" +
"    <meta name='viewport' content='width=device-width, initial-scale=1'>\n" +
"    <link rel='stylesheet' type='text/css' media='screen' href='css/productos.css'>\n" +
"    <script src='main.js'></script>\n" +
"</head>\n" +
"<body>\n" +
"    <header>  <div id=\"logos\"></div>\n" +
"            <div id=\"logos1\"></div>\n" +
"            <div id=\"logotitulo\"></div>\n" +
"     </header>\n" +
"    <main>"
        + "<div class=\"wrap category-page\" ng-controller=\"Application.Controllers.Category.ProductList\">");
       
        String consultaSql = "SELECT * FROM producto  ORDER BY `pro_id` DESC LIMIT 12";
        Connection miConexion = null;
        try {
            miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSql);
            ResultSet miResultado = miPreparativo.executeQuery();
            while (miResultado.next()) {
                productos producto1 = new productos();
                producto1.pro_id = miResultado.getString("pro_id");
                producto1.pro_nom = miResultado.getString("pro_nom");
                producto1.pro_pre = miResultado.getString("pro_pre");
                producto1.pro_fot = miResultado.getString("pro_fot");
                producto1.pro_des = miResultado.getString("pro_des");
                System.out.println(producto1);

//                out.println("<p>" + miResultado.getString("pro_id") + " | "
//                       + miResultado.getString("pro_nom") + " | " 
//                       + miResultado.getString("pro_pre") + " | "
//                       + "</p>");

//              out.println("<article>");
//              
//                out.println("<div>N* Registro: " + producto1.pro_id + "</div>");
//                out.println("<div>Nombre: " + producto1.pro_nom + " </div>");
//                out.println("<div>Precio " + producto1.pro_pre + " </div>");
//                 out.println("<img src=' " + producto1.pro_fot +"' width='200' />");
//              
//                
//                out.println("</article>");
//                
                        
                out.println("<div class=\"products\">\n" +
"        <div class=\"product-box\"\n" +
"             ng-repeat=\"product in products\">\n" +
"          <div class='title'>" + producto1.pro_nom        + "          " + " </div>\n" +
"          <div class='show-base'>\n" +
"            <img  width='180' src=\"" + producto1.pro_fot + "\" alt=\"{{product.name}}\" />\n" +
"            <div class=\"mask\">\n" +
"              <h2>" + producto1.pro_nom + "</h2>\n" +
"              <p ng-class=\"{old:product.specialPrice}\">" + "$" + producto1.pro_pre + ".-" + " </p>\n" +
"              <p ng-show=\"product.specialPrice\">"+"desc  " +producto1.pro_des + "%." +"CTDO"+" </p>\n" +
"              <div class=\"description\">\n" +
"                " + producto1.pro_id + "\n" +
"              </div>\n" +
"              <div>\n" +
"                <a href=\"#\" class=\"more\">info</a>\n" +
"                <a href=\"#\" class=\"tocart\">Compra</a>\n" +
"              </div>\n" +
"            </div>\n" +
"          </div>\n" +
"        </div>\n" +
"      </div>");
            }

        } catch (ClassNotFoundException miExepcion) {
            System.out.println(miExepcion);
        } catch (SQLException miExepcion) {
            System.out.println(miExepcion);
        } finally {
            try {
                if (miConexion != null) {
                    miConexion.close();
                }

            } catch (SQLException miExepcion) {
                System.out.println(miExepcion);
            }
        }
            out.println("</main>\n" +
"    <footer></footer>\n" +
"</body>\n" +
"</html>");


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
