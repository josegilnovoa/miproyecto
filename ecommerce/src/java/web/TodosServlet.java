package web;

import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "TodosServlet", urlPatterns = {"/TodosServlet"})
public class TodosServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        //request.getParameter("pro_id");
        request.getParameter("pro_nom");
        request.getParameter("pro_pre");
        request.getParameter("pro_fot");
        request.getParameter("pro_des");

        productos productoNuevo = new productos();
        //productoNuevo.pro_id = request.getParameter("pro_id");
        productoNuevo.pro_nom = request.getParameter("pro_nom");
        productoNuevo.pro_pre = request.getParameter("pro_pre");
        productoNuevo.pro_fot = request.getParameter("pro_fot");
        productoNuevo.pro_des = request.getParameter("pro_des");
        productoNuevo.ValidaFoto();
        String consultaSql = "INSERT INTO `producto` ( `pro_nom`, `pro_pre` , `pro_fot`,`pro_des` ) VALUES " + "(" + "'" + productoNuevo.pro_nom + "'" + ", "
                + " " + "'" + productoNuevo.pro_pre + "'" + ", "
                + " " + "'" + productoNuevo.pro_fot + "'" + ", "
                + " " + "'" + productoNuevo.pro_des + "' );";
        //out.println(" tv de 40' " + "," + 73000 );
        out.println("Aca vas a subir el producto nuevo!");
        Connection miConexion = null;

        try {
            miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSql);//con esta renglon,  hacemos que se venga el total de productos 
            miPreparativo.executeQuery();
            out.println("producto agregado");
            response.sendRedirect(request.getContextPath() + "/ConsultaTvs");

        } catch (ClassNotFoundException | IOException | SQLException miExepcion) {
            System.out.println(miExepcion);
        } finally {
            try {
                if (miConexion != null) {
                    miConexion.close();
                }

            } catch (SQLException miExepcion) {
                out.println(miExepcion);
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
