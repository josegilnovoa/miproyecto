-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2020 a las 19:10:36
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ecommerce`
--
CREATE DATABASE IF NOT EXISTS `ecommerce` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ecommerce`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `los`
--

CREATE TABLE `los` (
  `los_id` int(11) NOT NULL,
  `los_int` varchar(20) NOT NULL COMMENT 'interesados',
  `los_num` int(5) NOT NULL COMMENT 'numero'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `los`
--

INSERT INTO `los` (`los_id`, `los_int`, `los_num`) VALUES
(1, 'hombre', 1),
(2, 'mujer', 2),
(3, 'adolescente', 3),
(4, 'nene', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `pro_id` int(5) NOT NULL COMMENT 'codigo',
  `pro_nom` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'nombre',
  `pro_pre` varchar(15) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'precio',
  `pro_fot` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL COMMENT 'foto de la prenda',
  `pro_des` varchar(20) NOT NULL COMMENT 'descuento'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`pro_id`, `pro_nom`, `pro_pre`, `pro_fot`, `pro_des`) VALUES
(1, 'zapatilla de andar', '34000', 'https://www.distritomoda.com.ar/sites/default/files/styles/producto_interior/public/imagenes/dsc_8653_1564672870.jpg?itok=QdXr5ypk', '20'),
(2, 'un pullover ', '7900', 'https://image.slidesharecdn.com/prendasdevestirsinfrases-180406202653/95/prendas-de-vestir-sin-frases-1-638.jpg?cb=1523046544', '10'),
(3, 'camilla man corta', '2800', 'https://www.dafiti.com.ar/Camisa-Natural-Quiksilver-Fluid-Geometric-Ss-411485.html', '25'),
(4, 'camilla man corta', '72000', 'https://www.dafiti.com.ar/Camisa-Natural-Quiksilver-Fluid-Geometric-Ss-411485.html', '15'),
(5, 'camilla manga corta', '5377', 'https://www.dogmaind.com/wp-content/uploads/2018/10/8278_modelo.jpg', '15'),
(6, 'camilla vestir', '3400', 'https://i.pinimg.com/originals/e4/94/4e/e4944ee3dccb9e9553b218affc7b6459.jpg', '0'),
(7, 'camisa sport', '5499', 'https://i.pinimg.com/originals/e4/94/4e/e4944ee3dccb9e9553b218affc7b6459.jpg', '0'),
(8, 'zapatilla runner', '23899', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSReWRriiLerpYN1ddOEnpVU8RoSxfwXi1HCQ&usqp=CAU', '20'),
(9, 'pullover lana', '54999', 'https://i.pinimg.com/736x/3f/00/aa/3f00aa21efb6cba1cbdf2d5b3ca802a4.jpg', '20'),
(10, 'pantalon sarga', '7600', 'https://http2.mlstatic.com/pantalones-cargo-gabardina-jogger-bolsillos-cintura-punos-D_NQ_NP_683534-MLA31478230215_072019-F.jpg', '15'),
(11, 'pantalon invierno', '23999', 'https://img.ltwebstatic.com/images2_pi/2019/09/09/15680185211843743766_thumbnail_600x799.webp', '23'),
(12, 'camisa de vestir ', '8500', 'https://www.distritomoda.com.ar/sites/default/files/styles/producto_interior/public/imagenes/dsc_8653_1564672870.jpg?itok=QdXr5ypk', '5'),
(13, 'pantalon inverno', '3600', 'https://i.pinimg.com/originals/c3/39/14/c339149de4a2ac189b914ac749b78b98.jpg', '20'),
(14, 'pantalon de mujer', '5499', 'https://i.pinimg.com/originals/c3/39/14/c339149de4a2ac189b914ac749b78b98.jpg', '14'),
(15, 'pantalon mujer ', '27900', 'https://www.distritomoda.com.ar/sites/default/files/styles/producto_interior/public/imagenes/dsc_8653_1564672870.jpg?itok=QdXr5ypk', '20'),
(16, 'Zapatilla mandalla', '7800', 'https://statics.glamit.com.ar/media/catalog/product/cache/39/image/600x900/9df78eab33525d08d6e5fb8d27136e95/r/m/rmu19p01475c_blanco_1.jpg', '0'),
(17, 'pantalon vaquero', '8000', 'https://i.pinimg.com/474x/9c/df/97/9cdf9726e9dc63f52b95b09201edc9fd.jpg', '12'),
(18, 'Zapatilla mandalla', '7800', 'https://statics.glamit.com.ar/media/catalog/product/cache/39/image/600x900/9df78eab33525d08d6e5fb8d27136e95/r/m/rmu19p01475c_blanco_1.jpg', '25'),
(19, 'zapatilla de mujer', '28000', 'https://ferreira.vteximg.com.br/arquivos/ids/309738-588-588/to_52328.jpg?v=637035029190870000', '15'),
(20, 'zapatilla fila', '4000', 'https://i.pinimg.com/originals/e4/94/4e/e4944ee3dccb9e9553b218affc7b6459.jpg', '0'),
(21, 'una pollera', '1899', 'https://http2.mlstatic.com/pollera-tableada-reflectiva-reflex-importada-D_NQ_NP_907296-MLA32770714922_112019-Q.jpg', '5'),
(22, 'zapatilla puma', '4900', 'https://d26lpennugtm8s.cloudfront.net/stores/001/166/416/products/3158662_61-4ca770b8ce65d7fd2a15887733722233-640-0.jpg', '15'),
(23, 'zapallia fortarrum ', '5600', 'https://www.digitalsport.com.ar/files/products/5d67cb04ef64f-481503-500x500.jpg', '5'),
(24, 'camisa hombre', '8000', 'https://d26lpennugtm8s.cloudfront.net/stores/652/531/products/781787-mla30165895383_042019-o-7a3af5dc35f1617a7415581347379433-640-0.jpg', '0'),
(25, 'camisa de sport', '2800', 'https://d26lpennugtm8s.cloudfront.net/stores/652/531/products/781787-mla30165895383_042019-o-7a3af5dc35f1617a7415581347379433-640-0.jpg', '10'),
(26, 'zapatilla fila', '4900', 'https://www.tiendafuencarral.com.ar/37677-large_default/w-air-max-270-bc-fu-nj.jpg', '15'),
(27, 'zapatilla wired', '73399', 'https://static.cybermonday.com.ar/uploads/megaofertas/1120/zapatilla-negra-puma-vikky-stacked-sd-adp-pu305sh99wcear-1.jpg', '5'),
(29, 'patalones raffa ', '68000', 'https://http2.mlstatic.com/pack-x2-pantalones-gabardina-cargo-bolsillos-punos-cintura-D_NQ_NP_945842-MLA31478802292_072019-Q.jpg', '15'),
(30, 'camilla mujer', '28000', 'https://tottomexico.vteximg.com.br/arquivos/ids/194816-1000-1000/Camisa-para-Mujer-Manga-Larga-Tulan-morado-tulan-zephir-mini-stripes_1.jpg', '15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `los`
--
ALTER TABLE `los`
  ADD PRIMARY KEY (`los_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`pro_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `los`
--
ALTER TABLE `los`
  MODIFY `los_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `pro_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'codigo', AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
